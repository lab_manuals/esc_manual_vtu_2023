/***************************************

Enter the Angle : 60
Enter the Number of terms : 12

Input Angle = 60	No of terms = 12
Calculated value is :
Sin(60)/Cos(60) = 1.73205

Built In function value is :
Sin(60)/Cos(60) = 1.73205
=========================================

Enter the Angle : 30
Enter the Number of terms : 3

Input Angle = 30	No of terms = 3
Calculated value is :
Sin(30)/Cos(30) = 0.577334

Built In function value is :
Sin(30)/Cos(30) = 0.57735
=========================================

Enter the Angle : 45
Enter the Number of terms : 11

Input Angle = 45	No of terms = 11
Calculated value is :
Sin(45)/Cos(45) = 1

Built In function value is :
Sin(45)/Cos(45) = 1
***************************************/
