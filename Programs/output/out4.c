/***************************************

Enter the atomocity(x) of Element1 : 2
Enter the atomocity(y) of Element2 : 2
Enter the atomocity(p) of Element1 in the compound : 2
Enter the atomocity(q) of Element2 in the compound : 1

x = 2	y = 2	p = 2	q = 1

b1 = 2	b2 = 1	b3 = 2

Balanced Equation is now :
	2*2 + 1*2 ==> 2(2,1)


Enter the atomocity(x) of Element1 : 2
Enter the atomocity(y) of Element2 : 3
Enter the atomocity(p) of Element1 in the compound : 4
Enter the atomocity(q) of Element2 in the compound : 5

x = 2	y = 3	p = 4	q = 5

b1 = 6	b2 = 5	b3 = 3

Balanced Equation is now :
	6*2 + 5*3 ==> 3(4,5)

***************************************/

