/***************************************
*************************************************************
*	Program to find Mechanical Energy of a body	    *
*************************************************************
Enter the mass (in kg) of the body: 80   

Enter the height (in metres) of the body: 10

Enter the velocity (in meters per second) of the body: 10

Potential energy associated with the body is 7844.800 Joules

Kinetic energy associated with the body is 4000.000 Joules

Total energy associated with the body is 11844.800 Joules

***************************************/
