/***************************************************************************
*File			: A10MeanVarianceSD.c
*Description	: Program to compute Mean, Variance and Standard Deviation
					using pointer to an array
*Author		: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 22.04
*Date			: 10 August 2022
***************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
/***************************************************************************
*Function			: 	main
*Input parameters	:	no parameters
*RETURNS			:	0 on success
***************************************************************************/
int main(void)
{
    int i,iNum;
    float fMean = 0.0f, fVariance = 0.0f, fSd = 0.0f,faArray[100],fSum=0.0f;
    float *fptr;

    printf("\nEnter the number of Values : ");
    scanf("%d",&iNum);
    fptr = faArray; 
/*    fptr = (float*)malloc(iNum*sizeof(float));*/
    printf("\nEnter %d values\n", iNum);
    for(i=0; i<iNum; i++)
    {
        scanf("%f",fptr+i);
        fSum += *(fptr+i);		//fSum += fptr[i]; this is also valid
    }
    fMean = fSum/iNum;

    for(i=0; i<iNum; i++)
    {
        fVariance += (fptr[i] - fMean)*(fptr[i] - fMean);
		//fVariance += (*(fptr+i) - fMean)*(*(fptr+i) - fMean);
    }
    fVariance /= iNum;
    fSd = sqrt(fVariance);
    printf("\nThe values entered are");
    for(i=0; i<iNum; i++)
    {
        printf("\t%g",fptr[i]);        //printf("\n\t%f",*(fptr+i));
    }
    printf("\n");

    printf("\n**************************************\n");
    printf("\tSum\t = \t%g\n\tMean\t = \t%g\n\tVariance = \t%g\nStandard Deviation = \t%g",fSum,fMean,fVariance,fSd);
    printf("\n**************************************\n");
    return 0;
}

