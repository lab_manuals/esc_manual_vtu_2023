/***************************************************************************
*File			: A02DistConvert.c
*Description	: Program to convert Kilometers into Meters and Centimeters.
*Author			: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 22.04
*Date			: 02 December 2022
***************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
/***************************************************************************
*Function			: 	main
*Input parameters	:	no parameters
*RETURNS			:	0 on success
***************************************************************************/
int main(void)
{
	double dDistKm, dDistMtr, dDistCm;


	printf("\n*************************************************************");
	printf("\n*\tProgram to convert Kilometers into Meters and Centimeters\t    *\n");
	printf("*************************************************************");

	printf("\nEnter the distance in kilometers : ");	scanf("%lf",&dDistKm);

	dDistMtr = dDistKm * 1000;
	dDistCm = dDistMtr * 100;
	
	printf("\nThe distance entered in kilometers is : %0.3lf \n", dDistKm);
	printf("\nEquivalent distance in meters is : %0.3lf \n", dDistMtr);
	printf("\nEquivalent distance in centimeters is : %0.3lf \n", dDistCm);
	return 0;
}

