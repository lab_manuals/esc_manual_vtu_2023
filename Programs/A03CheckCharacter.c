/***************************************************************************
*File			: A03CheckCharacter.c
*Description	: Program to check the given character is Lowercase or Uppercase or Special character.
*Author		: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 22.04
*Date			: 10 August 2022
***************************************************************************/
#include<stdio.h>
#include<stdlib.h>
/***************************************************************************
*Function		: 	main
*Input parameters	:	no parameters
*RETURNS		:	0 on success
***************************************************************************/
int main(void)
{
	char cChar;
	printf("\nEnter a character to be checked : "); 	scanf("%c", &cChar);
	
	if(cChar >= 'a' && cChar <= 'z')
	{
		printf("\nThe character entered is a lower case character\n");
	}
	else if(cChar >= 'A' && cChar <= 'Z')
	{
		printf("\nThe character entered is a upper case character\n");
	}
	else if(cChar >= '0' && cChar <= '9')
	{
		printf("\nThe character entered is a digit\n");
	}
	else
	{
		printf("\nThe character entered is a special character\n");
	}

	return 0;
}
