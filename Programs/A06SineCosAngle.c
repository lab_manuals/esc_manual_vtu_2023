/***************************************************************************
*File			: A06SineCosAngle.c
*Description	: Program to calculate Sin(x)/Cos(x) using Taylor series
*Author		: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 22.04
*Date			: 10 August 2022
***************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include <math.h>
/***************************************************************************
*Function			: 	main
*Input parameters	:	no parameters
*RETURNS			:	0 on success
***************************************************************************/
int main()
{
	float fAngD, fAngR;
	float fTerm, fNum, fDen, fSVal,fCVal;
	int i,iNum;
	printf("\nEnter the Angle : ");	scanf("%f",&fAngD);
	printf("\nEnter the Number of terms : ");		scanf("%d",&iNum);
	printf("\nInput Angle = %g\n",fAngD);	
	printf("No of terms = %d\n",iNum);

	fAngR= (fAngD*M_PI)/180 ;
	
	//Calculation of Sine of an angle using Taylor's series
	fNum=fAngR;
	fDen=1.0;
	fSVal =0.0;
	fTerm=fNum/fDen;
	for(i=1;i<=iNum;i++)
	{
		fSVal = fSVal + fTerm;
		fNum = -fNum * fAngR * fAngR ;
		fDen = fDen * (2*i) * (2*i+1);
		fTerm = fNum/fDen;
	}

	//Calculation of Cosine of an angle using Taylor's series
	fNum=1.0;
	fDen=1.0;
	fCVal =0.0;
	fTerm=1.0;
	for(i=1;i<=iNum;i++)
	{
		fCVal = fCVal + fTerm;
		fNum = -fNum * fAngR * fAngR ;
		fDen = fDen * (2*i) * (2*i-1);
		fTerm = fNum/fDen;
	}
	
	printf("\nCalculated value is :\nSin(%g)/Cos(%g) = %g\n",fAngD, fAngD, fSVal/fCVal);
	printf("\nBuilt In function value is :\nSin(%g)/Cos(%g) = %g\n", fAngD, fAngD, sin(fAngR)/cos(fAngR));

	return 0;
}
